import { takeLatest, put } from "redux-saga/effects";
import { 
    CONTACT_LIST_PROCESS,
    CONTACT_LIST_SUCCESS,
    CONTACT_LIST_ERROR,
    CONTACT_DETAIL_PROCESS,
    CONTACT_DETAIL_SUCCESS,
    CONTACT_DETAIL_ERROR,
    CONTACT_DELETE_PROCESS,
    CONTACT_DELETE_SUCCESS,
    CONTACT_DELETE_ERROR,
    CONTACT_ADD_PROCESS,
    CONTACT_ADD_SUCCESS,
    CONTACT_ADD_ERROR,
    CONTACT_UPDATE_PROCESS,
    CONTACT_UPDATE_SUCCESS,
    CONTACT_UPDATE_ERROR
} from "../../actions/contact";
import { filterFetch } from "../../utils/apiFetch";
import { URL_API } from "../../utils/constant"

function* contactList() {
  try {
    const result = yield filterFetch(URL_API + 'contact', {
      method: 'GET'
    });
    yield put({
      type: CONTACT_LIST_SUCCESS,
      result: result
    });
  } catch (error) {
    yield put({
      type: CONTACT_LIST_ERROR,
      error: error
    });
  }
}

function* contactDetail(action) {
  try {
    const result = yield filterFetch(URL_API + `contact/${action.data}`, {
      method: 'GET'
    });
    yield put({
      type: CONTACT_DETAIL_SUCCESS,
      result: result
    });
  } catch (error) {
    yield put({
      type: CONTACT_DETAIL_ERROR,
      error: error
    });
  }
}

function* contactDelete(action) {
  try {
    console.log(action.data)
    const result = yield filterFetch(URL_API + `contact/${action.data}`, {
      method: 'DELETE'
    });
    yield put({
      type: CONTACT_DELETE_SUCCESS,
      result: result
    });
  } catch (error) {
    yield put({
      type: CONTACT_DELETE_ERROR,
      error: error
    });
  }
}

function* contactAdd(action) {
  try {
    const result = yield filterFetch(URL_API + 'contact', {
      method: 'POST',
      body: JSON.stringify(action.data)
    });
    yield put({
      type: CONTACT_ADD_SUCCESS,
      result: result
    });
  } catch (error) {
    yield put({
      type: CONTACT_ADD_ERROR,
      error: error
    });
  }
}

function* contactUpdate(action) {
  try {
    const result = yield filterFetch(URL_API + `contact/${action.param}`, {
      method: 'PUT',
      body: JSON.stringify(action.data)
    });
    yield put({
      type: CONTACT_UPDATE_SUCCESS,
      result: result
    });
  } catch (error) {
    yield put({
      type: CONTACT_UPDATE_ERROR,
      error: error
    });
  }
}

export function* contactListWatch() {
  yield takeLatest(CONTACT_LIST_PROCESS, contactList);
}

export function* contactDetailWatch() {
  yield takeLatest(CONTACT_DETAIL_PROCESS, contactDetail);
}

export function* contactDeleteWatch() {
  yield takeLatest(CONTACT_DELETE_PROCESS, contactDelete);
}

export function* contactAddWatch() {
  yield takeLatest(CONTACT_ADD_PROCESS, contactAdd);
}

export function* contactUpdateWatch() {
  yield takeLatest(CONTACT_UPDATE_PROCESS, contactUpdate);
}