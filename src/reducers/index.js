import {combineReducers} from 'redux';
import {reducer as formReducer} from 'redux-form';

import {contactList, contactDetail, contactDelete, contactAdd, contactUpdate} from "./contact"

const allReducers = combineReducers({
  contactList,
  contactDetail,
  contactDelete,
  contactAdd,
  contactUpdate,
  
  form: formReducer,
});
export default allReducers;
