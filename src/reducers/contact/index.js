import {
    CONTACT_LIST_PROCESS,
    CONTACT_LIST_SUCCESS,
    CONTACT_LIST_ERROR,
    CONTACT_DETAIL_PROCESS,
    CONTACT_DETAIL_SUCCESS,
    CONTACT_DETAIL_ERROR,
    CONTACT_DELETE_PROCESS,
    CONTACT_DELETE_SUCCESS,
    CONTACT_DELETE_ERROR,
    CONTACT_ADD_PROCESS,
    CONTACT_ADD_SUCCESS,
    CONTACT_ADD_ERROR,
    CONTACT_UPDATE_PROCESS,
    CONTACT_UPDATE_SUCCESS,
    CONTACT_UPDATE_ERROR
} from "../../actions/contact";

const initState = {
  result: null,
  loading: false
};

const initStates = {
    result: [],
    loading: false
};
  
export function contactList(state = initStates, action) {
  switch (action.type) {
    case CONTACT_LIST_PROCESS:
      return {
        ...state,
        loading: true,
        error: null,
        result: []
      };
    case CONTACT_LIST_SUCCESS:
      return {
        ...state,
        result: action.result.data,
        loading: false,
        error: null
      };
    case CONTACT_LIST_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: []
      };
    default:
      return state;
  }
}

export function contactDetail(state = initState, action) {
  switch (action.type) {
    case CONTACT_DETAIL_PROCESS:
      return {
        ...state,
        loading: true,
        error: null,
        result: null
      };
    case CONTACT_DETAIL_SUCCESS:
      return {
        ...state,
        result: action.result.data,
        loading: false,
        error: null
      };
    case CONTACT_DETAIL_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null
      };
    default:
      return state;
  }
}

export function contactDelete(state = initState, action) {
  switch (action.type) {
    case CONTACT_DELETE_PROCESS:
      return {
        ...state,
        loading: true,
        error: null,
        result: null
      };
    case CONTACT_DELETE_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null
      };
    case CONTACT_DELETE_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null
      };
    default:
      return state;
  }
}

export function contactAdd(state = initState, action) {
  switch (action.type) {
    case CONTACT_ADD_PROCESS:
      return {
        ...state,
        loading: true,
        error: null,
        result: null
      };
    case CONTACT_ADD_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null
      };
    case CONTACT_ADD_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null
      };
    default:
      return state;
  }
}

export function contactUpdate(state = initState, action) {
  switch (action.type) {
    case CONTACT_UPDATE_PROCESS:
      return {
        ...state,
        loading: true,
        error: null,
        result: null
      };
    case CONTACT_UPDATE_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null
      };
    case CONTACT_UPDATE_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null
      };
    default:
      return state;
  }
}