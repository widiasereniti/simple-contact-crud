import moment from 'moment';
import AliyunOSS from 'aliyun-oss-react-native';
import {
  bucketname,
  secretKey,
  accessKey,
  endPoint,
  configuration,
} from './constant';

// / / Configure AccessKey according to AliyunOss
AliyunOSS.initWithPlainTextAccessKey(
  accessKey,
  secretKey,
  endPoint,
  configuration,
);

const urlCdn = 'https://baf-mobile-dev.oss-ap-southeast-5.aliyuncs.com/';

export const uploadOssFile = async (filepath) => {
  const filetype = filepath.substring(filepath.lastIndexOf('.')).toLowerCase();
  //   / / Get the image suffix

  const currm = moment(new Date());
  const oo = Math.random();
  const objectKey = `profile_pic/${currm.format(
    'YYYYMMDD',
  )}/${currm}${oo}${filetype}`;
  //   / / Generate objectKey as a custom path
  return AliyunOSS.asyncUpload(bucketname, objectKey, filepath)
    .then((result) => {
      return `${urlCdn}${objectKey}`;
    })
    .catch((error) => {
      console.log('=== error', error);
    });
};
