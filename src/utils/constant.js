export const URL_API = 'https://simple-contact-crud.herokuapp.com/';

export const endPoint = 'https://oss-ap-southeast-5.aliyuncs.com';
export const accessKey = '********';
export const secretKey = '********';
export const bucketname = 'contact-crud';

export const configuration = {
  maxRetryCount: 3,
  timeoutIntervalForRequest: 30,
  timeoutIntervalForResource: 24 * 60 * 60,
};