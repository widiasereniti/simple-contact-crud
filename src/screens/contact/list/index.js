import React, { Component } from 'react';
import { BackHandler, Alert, View, ScrollView, RefreshControl} from 'react-native';
import { Text, Container, Spinner, Content, Fab, Icon } from 'native-base';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import MenuHeader from "../../../components/menuHeader";
import { contactList, contactDelete } from "../../../actions/contact";
import ContactCardList from "../../../components/contact/contactCardList";
import FilterSearch from "../../../components/filterSearch"
import {createFilter} from 'react-native-search-filter';
import styles from './style';

class ContactList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screen: 'List Kontak',
      searchTerm: '',
    };
  }

  componentDidMount = async () => {
    const { contactList } = this.props;
    BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);

    contactList();
  }

  handleBackButton = () => {
    if (this.props.navigation.isFocused()) {
        Alert.alert(
            'Keluar Aplikasi',
            'Apakah Anda yakin?', [{
                text: 'Tidak',
                style: 'cancel'
            }, {
                text: 'Ya',
                onPress: () => BackHandler.exitApp()
            }, ], {
                cancelable: false
            }
        )
        return true;
    }
  }

  componentDidUpdate = async (prevProps, prevState) => {
    const { contactListError, contactDeleteResult, contactDeleteError, contactList } = this.props;

    if (contactListError && prevProps.contactListError !== contactListError) {
      Alert.alert('Gagal', contactListError.message);
    }

    if (contactDeleteError && prevProps.contactDeleteError !== contactDeleteError) {
      Alert.alert('Gagal', contactDeleteError.message);
    }
    
    if (contactDeleteResult && prevProps.contactDeleteResult !== contactDeleteResult) {
      Alert.alert('Berhasil', contactDeleteResult.message);
      contactList();
    } 
  }

  defaultTemplate = () => {
    const { contactListLoading, contactListError } = this.props;

    return contactListLoading ? 
      <Spinner color="#002f5f" />
     : 
      <View style={{marginVertical:'20%', alignItems:'center'}}>
        <Text style={{fontSize:14, color:'grey'}}>Data not found</Text>
      </View>;
  };

  getContactList = (data, index) => {
    return (
      <ContactCardList
          data={data}
          index={index}
          key={data.id}
          handleDelete={() => this.handleDelete(data.id)}
          handleDetail={() => this.handleDetail(data.id)}
      />
      );
    };

  onChangeText = (term) => {
    this.searchUpdated(term);
  };

  searchUpdated = (term) => {
    this.setState({searchTerm: term});
  };

  handleDelete = (data) => {
    const {contactDelete} = this.props;

    Alert.alert(
      'Hapus Kontak',
      'Apakah Anda yakin?', [{
          text: 'Tidak',
          style: 'cancel'
      }, {
          text: 'Ya',
          
          onPress: () => contactDelete(data)
      }, ], {
          cancelable: false
      }
    )
  }

  handleDetail = (data) => {
    this.props.navigation.navigate('ContactDetail', {id:data})
  }

  render() {
    const { contactListResult, contactList } = this.props;
    const KEYS_TO_FILTERS = ['firstName'];
    const filteredData = contactListResult.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS));
    
    return (
      <Container>
          <MenuHeader
            title={this.state.screen}
            goBack={this.goBack}
          />
          <View style={styles.viewHeader}>
            <FilterSearch
                onChangeText={this.onChangeText}
                placeholer="Cari Kontak..."
            />
          </View>
          <ScrollView style={styles.ScrollView} refreshControl={<RefreshControl refreshing={false} onRefresh={() => contactList()}/>}>
            {filteredData.length > 0 ? 
                filteredData.map((data, index) => this.getContactList(data, index)) : this.defaultTemplate()}
            <View style={styles.ScrollViewBottom} />
          </ScrollView>
          <Fab
            active={this.state.active}
            direction="up"
            containerStyle={{ marginVertical:'5%', marginHorizontal:'2%'}}
            style={{ backgroundColor: '#5CB95D'}}
            position="bottomRight"
            onPress={() => {this.props.navigation.navigate('ContactDetail')}}
          >
            <Text style={{color:'white'}}>+</Text>
          </Fab>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    contactListResult: state.contactList.result,
    contactListLoading: state.contactList.loading,
    contactListError: state.contactList.error,
    contactDeleteResult: state.contactDelete.result,
    contactDeleteLoading: state.contactDelete.loading,
    contactDeleteError: state.contactDelete.error,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    contactList,
    contactDelete
   }, dispatch);
}

export default connect(
  mapStateToProps,
  matchDispatchToProps
)(ContactList);
