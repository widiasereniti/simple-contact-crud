import React, {Component} from 'react';
import SearchInput from 'react-native-search-filter';
import styles from "./style"

export default class FilterSearch extends Component {
    constructor(props) {
      super(props);
      this.state = {
        data: [],
      };
    }
  
    render() {
      const {onChangeText, placeholder} = this.props;
      
      return (
        <SearchInput
                style={styles.SearchInput}
                onChangeText={onChangeText}
                placeholder={placeholder}
        />
      );
    }
  }