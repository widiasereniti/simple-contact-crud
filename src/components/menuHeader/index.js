import React, { Component } from "react";
import { Platform, StatusBar, TouchableOpacity } from "react-native";
import { Container, Header, Left, Body, Right, Button, Title, Text,Icon, View } from 'native-base';
import styles from './style';
export default class MenuHeader extends Component {
  render() {
    return (
    <View>
        <Header style={styles.Header}>
          <StatusBar backgroundColor="white" barStyle="dark-content"/>
          <Left>
            {this.props.title == 'List Kontak' ? null : 
            <TouchableOpacity onPress={this.props.goBack} style={{marginLeft:'5%'}}>
              <Text style={styles.textBack}>Back</Text>
            </TouchableOpacity>
            }
          </Left>
          <Body style={{position:'absolute'}}>
            <Text style={styles.title}>{this.props.title}</Text>
          </Body>
          <Right>
            <Button hasText transparent>
            </Button>
          </Right>
        </Header>  
      </View>
    );
  }
}