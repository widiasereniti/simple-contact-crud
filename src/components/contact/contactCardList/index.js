import React, {Component} from 'react';
import {Text, View, TouchableOpacity, Image} from 'react-native';
import styles from './style';

export default class ContactCardList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }

  btnDelete = (data, index) => {
    const {handleDelete, handleDetail} = this.props

    return (
      <View style={{flex:1, flexDirection:'row', justifyContent:'space-between'}}>
        <TouchableOpacity
          key={'item-' + index}
          style={styles.BtnBlue}
          onPress={handleDetail}
        >
          <Text style={{fontSize: 14, color: 'white'}}>
            Edit
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          key={'item-' + index}
          style={styles.BtnRed}
          onPress={handleDelete}
        >
          <Text style={{fontSize: 14, color: 'white'}}>
            Hapus
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    const {index, data, handleDetail} = this.props;

    return (
      <TouchableOpacity
        style={styles.Content}
        key={'item-' + data.id}
      >
        <View style={styles.CardHeader}>
          <View style={styles.HeaderOrderNo}>
            <Text style={styles.TextOrderNo}>{data.firstName + ` ` + data.lastName}</Text>
          </View>
        </View>
        <View style={styles.CardBody}>
          <View style={styles.TopBodyContent}>
            
            <View style={styles.FullnameContainer}>
              {data.photo == 'N/A' ? null :
                <Image
                  source={{uri: data.photo}}
                  style={{width:55, height:55, borderRadius:5}}
                />
              }
            </View>
            <View style={styles.IdNoContainer}>
              <Text style={styles.TextFullname}>{`Age : `+ data.age}</Text>
            </View>
          </View>

          <View style={styles.MiddleBodyContent}>
            {this.btnDelete(data, index)}
          </View>
          <View style={styles.BottomBodyContent}>
            
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}
