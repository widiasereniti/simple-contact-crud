import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    Content : {
        width:"94%",
        backgroundColor:"white",
        height:100,
        alignSelf:'center',
        borderRadius:5,
        marginVertical:8,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.3,
        shadowRadius: 3,
        elevation: 5
    },
    CardHeader : {
        width:"100%",
        backgroundColor:'#053362',
        flexDirection:'row',
        justifyContent:'space-between',
        height:"30%",
        alignSelf:'center',
        borderTopLeftRadius:5,
        borderTopRightRadius:5,
        borderBottomLeftRadius:0,
        borderBottomLeftRadius:0
    },
    HeaderOrderNo : {
        marginTop:7,
        marginHorizontal:10
    },
    TextOrderNo : {
        fontSize:14,
        color:'white'
    },
    DateHeader : {
        marginTop:11,
        marginHorizontal:10
    },
    TextDate : {
        fontSize:10,
        color:'white'
    },
    CardBody : {
        width:'100%',
        height:'78%',
        borderBottomEndRadius:5
    },
    TopBodyContent : {
        width:'100%',
        height:"25%",
        flexDirection:'row',
        justifyContent:'space-between'
    },
    FullnameContainer : {
        marginTop:7,
        marginHorizontal:10
    },
    TextFullname : {
        fontSize:12,
        color:'#053362',
        alignSelf:'flex-end',
    },
    IdNoContainer : {
        marginTop:8,
        marginHorizontal:10
    },
    TextIdNo : {
        fontSize:12,
        color:'#053362'
    },
    MiddleBodyContent : {
        width:'50%',
        height:"25%",
        marginTop:10,
        marginHorizontal:10,
        alignSelf:'flex-end'
    },
    OrderStatDescContainer : {
        marginTop:2,
        marginHorizontal:10
    },
    TextOrderStatDesc : {
        fontSize:10,
        color:'#606060'
    },
    BottomBodyContent : {
        width:'100%',
        height:"10%",
        flexDirection:'row'
    },
    AppSourceContainer : {
        marginTop:-8,
        marginHorizontal:10
    },
    TextAppSource : {
        fontSize:12
    },
    BtnBlue: {
        marginTop:8,
        width:"47%",
        paddingHorizontal:10,
        height:30,
        backgroundColor:'#347AB7',
        alignSelf:'center',
        borderRadius:5,
        alignItems:'center',
        justifyContent:'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.3,
        shadowRadius: 3,
        elevation: 5
    },
    BtnRed: {
        marginTop:8,
        width:"47%",
        paddingHorizontal:10,
        height:30,
        backgroundColor:'#d9534f',
        alignSelf:'center',
        borderRadius:5,
        alignItems:'center',
        justifyContent:'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.3,
        shadowRadius: 3,
        elevation: 5
    },
})