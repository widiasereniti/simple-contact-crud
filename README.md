# Simple Contact CRUD

Setup for android :
1. Setup react-native CLI environment seperti di website https://reactnative.dev/docs/environment-setup
2. npm install
3. react-native run-android

Setup for ios :
1. Setup react-native CLI environment seperti di website https://reactnative.dev/docs/environment-setup
2. npm install
3. cd ios
4. pod install
5. react-native run-ios

Environment :
1. UI using native-base
2. State management using redux
3. Middleware using redux-saga
4. Form management using redux-form
5. The unit test is not implemented yet

# Lets try



